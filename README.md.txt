[![CI](https://github.com/tdulcet/compact-dictionaries/actions/workflows/ci.yml/badge.svg)](https://github.com/tdulcet/compact-dictionaries/actions/workflows/ci.yml)
[![pipeline status](https://gitlab.com/tdulcet/compact-dictionaries/badges/main/pipeline.svg)](https://gitlab.com/tdulcet/compact-dictionaries/-/commits/main)

# Compact Dictionaries
Compact dictionaries in English that automatically update weekly.

Copyright © 2023 Teal Dulcet

Preprocessed free dictionaries/thesauruses in [JSON Lines](https://jsonlines.org/) format that are automatically updated weekly. The dictionaries include the [part of speech](https://en.wikipedia.org/wiki/Part_of_speech), definitions ([senses](https://en.wikipedia.org/wiki/Word_sense)), forms of the word, [synonyms](https://en.wikipedia.org/wiki/Synonym), [antonyms](https://en.wikipedia.org/wiki/Opposite_(semantics)), pronunciation and more information.

All dictionaries are provided uncompressed and in a compact JSON format with minimal single character keys and no whitespace. They include much more information then would be found in a traditional paper dictionary or thesaurus, including the full list of meanings for each word. While the definitions are currently in English, they are available with words in over 100 languages. The dictionaries are designed so that applications can directly download them, without developers needing to release an entire software update. This allows users to enjoy much more frequent updates and thus more accurate information.

❤️ Please visit [tealdulcet.com](https://www.tealdulcet.com/) to support this project and my other software development.

The dictionaries are hosted [on GitLab](https://gitlab.com/tdulcet/compact-dictionaries) because while it now has a [100 MiB file size limit](https://docs.gitlab.com/ee/user/free_push_limit.html) for regular files, it has no maximum file size for Git Large File Storage (LFS) files, just a [10 GiB repository size limit](https://en.wikipedia.org/w/index.php?title=GitLab&oldid=1104375442#Repository_size_limits). In contrast, GitHub has a [100 MiB file size limit](https://docs.github.com/en/repositories/working-with-files/managing-large-files/about-large-files-on-github) and [strict bandwidth limits](https://docs.github.com/en/repositories/working-with-files/managing-large-files/about-storage-and-bandwidth-usage) on Git LFS files. Commits older than one month (previously one year) are automatically squashed to keep the repository size under that limit. Please see the [CHANGELOG](CHANGELOG.md) for the full history. The dictionaries are now updated [on GitHub](https://github.com/tdulcet/compact-dictionaries) as it has no limit for CI minutes for public repositories. In contrast, GitLab has a [400 CI minutes/month limit](https://about.gitlab.com/blog/2020/09/01/ci-minutes-update-free-users/).

## Dictionary comparison

| Dictionary | License | Updated | Download |
| --- | --- | --- | --- |
TABLE

## Dictionaries

### Wiktionary
Uses the [English Wiktionary](https://en.wiktionary.org/) dictionary data. It is created from the [Wiktionary dumps](https://dumps.wikimedia.org/enwiktionary/latest/), which is converted to a JSON Lines format by [kaikki.org](https://kaikki.org/dictionary/) using their open source [Wiktextract](https://github.com/tatuylonen/wiktextract) tool. See the [Wiktextract paper](http://www.lrec-conf.org/proceedings/lrec2022/pdf/2022.lrec-1.140.pdf) for more information. The resulting over 15 GiB file for all languages combined is then preprocessed to create a minimal dictionary for each language using the scripts in this repository. The English Wiktionary currently includes words in over 4,400 languages, so the scripts automatically select the around 100 languages [supported by Mozilla](https://mozilla-l10n.github.io/firefox-languages/) (Firefox and/or Thunderbird) or those with 50,000 words or more. This includes most modern languages, as well as Latin. The underlining Wiktionary dump files are updated monthly, but kaikki.org updates the extracted JSON files weekly to incorporate improvements made to their Wiktextract tool. If users notice any errors in the data, they should correct them by directly editing Wiktionary and this will automatically be included in the next monthly update.

Licensed under both the [Creative Commons Attribution-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/) (CC BY-SA 3.0) and the [GNU Free Documentation License](https://gnu.org/licenses/gfdl.html) (GFDL), so users must attribute it to Wiktionary.

#### JSON format
Uses the [JSON Lines](https://jsonlines.org/) format, where each line is a [JSON](https://json.org/) object for a word in the dictionary. Each JSON object may have the following keys:
* `""` (empty string) - String with the word (required)
* `"p"` - Array of strings with the parts of speech (POS) (required)
* `"d"` - Array of strings with the definitions of the word (required)
* `"f"` - Array of strings with the forms of the word (optional)
* `"s"` - Array of strings with the synonyms of the word (optional)
* `"n"` - Array of strings with the antonyms of the word (optional)
* `"i"` - String with the [International Phonetic Alphabet](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet) (IPA) pronunciation (optional)
* `"a"` - String with the filename for the pronunciation audio file in [OGG Vorbis format](https://en.wikipedia.org/wiki/Ogg) (`.ogg`), add the `https://upload.wikimedia.org/wikipedia/commons/` prefix to get the full URL (optional)
* `"w"` - Array of strings with the titles of the Wikipedia pages about the word, possibly prefixed with a language ID (optional)

Words, forms, synonyms and antonyms with any whitespace characters are excluded, as well as some POS categories that are not words, such as "character", "symbol", "prefix" and "suffix".

## JSON format

See above for the specific format of each dictionary.

## Contributing

Merge requests welcome! Ideas for contributions:

* Improve the performance of the update scripts.
* Reduce the size of the dictionaries.
* Provide localized versions of the dictionaries.
* Add more dictionaries.
