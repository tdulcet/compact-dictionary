#!/bin/bash

# Teal Dulcet
# Update dictionaries
# ./update.sh

# Output JSON files
OUTPUT=dictionary.json

# GitLab repository base URL
URL=https://gitlab.com/tdulcet/compact-dictionary

# Directories
DIRS=(wiktionary)

# Files
FILES=(kaikki.org-dictionary-all.jsonl)

set -e

if [[ $# -ne 0 ]]; then
	echo "Usage: $0" >&2
	exit 1
fi

# wget arguments
args=(-nv -T 30 --retry-connrefused --retry-on-host-error -c --content-disposition --compression auto)

# outputupdate <file>
outputupdate() {
	date -ud "@$(stat -c %Y -- "$1")" +%F
}

# Auto-scale number to unit
# Adapted from: https://github.com/tdulcet/Numbers-Tool/blob/master/numbers.cpp
# outputunit <number> [scale_base]
# scale_base:
	# 0=scale_IEC_I
	# 1=scale_SI
outputunit() {
	echo "$*" | awk 'BEGIN { suffix[0]=""; suffix[1]="K"; suffix[2]="M"; suffix[3]="G"; suffix[4]="T"; suffix[5]="P"; suffix[6]="E"; suffix[7]="Z"; suffix[8]="Y"; suffix[9]="R"; suffix[10]="Q" } function abs(x) { return x<0 ? -x : x } { number=$1; if ($2) scale_base=1000; else scale_base=1024; power=0; while (abs(number)>=scale_base) { ++power; number /= scale_base } anumber=abs(number); anumber += anumber<10 ? 0.0005 : (anumber<100 ? 0.005 : (anumber<1000 ? 0.05 : 0.5)); if (number!=0 && anumber<1000 && power>0) { str=sprintf("%'"'"'.15g", number); alength=5 + (number<0 ? 1 : 0); if (length(str) > alength) { prec=anumber<10 ? 3 : (anumber<100 ? 2 : 1); str=sprintf("%'"'"'.*f", prec, number) } } else str=sprintf("%'"'"'.0f", number); if (power<length(suffix)) str=str suffix[power]; else str=str "(error)"; if (! $2  && power>0) str=str "i"; print str }'
}

# outputfile <dir> <files>...
outputfile() {
	for i in "${@:2}"; do
		size=$(du -b -- "$i" | awk '{ print $1 }')
		words=$(wc -l -- "$i" | awk '{ print $1 }')
		code=${i%.*}
		code=${code#*-}
		echo -n "${LANGUAGES[$code]:+${LANGUAGES[$code]} ($code)<br>}⬇️ **[$i]($URL/-/raw/main/$1/$i?inline=false)**<br>$(outputunit "$size" 0)B$([[ $size -ge 1000 ]] && echo " ($(outputunit "$size" 1)B)") – $(printf "%'d" "$words") word$([[ $words -gt 1 ]] && echo "s")<br><small><details><summary>Checksums (click to show)</summary><pre>MD5: $(md5sum -- "$i" | awk '{ print $1 }')<br>SHA1: $(sha1sum -- "$i" | awk '{ print $1 }')<br>SHA256: $(sha256sum -- "$i" | awk '{ print $1 }')</pre></details></small>"
	done
}

# headtail <file>
headtail() {
	head "$1"
	echo "..."
	tail "$1"
}

# https://product-details.mozilla.org/1.0/languages.json
wget "${args[@]}" -O all-locales-Firefox https://hg.mozilla.org/mozilla-central/raw-file/default/browser/locales/all-locales
wget "${args[@]}" -O all-locales-Firefox-Android https://hg.mozilla.org/mozilla-central/raw-file/default/mobile/android/locales/all-locales
wget "${args[@]}" -O all-locales-Thunderbird https://hg.mozilla.org/comm-central/raw-file/default/mail/locales/all-locales

echo 'en-US' | sort -u - all-locales-* >all-locales.txt

(
	set -e
	trap 'echo "::error::Wiktionary failed to update"' ERR

	DIR=${DIRS[0]}
	FILE=${FILES[0]}

	echo -e "\nWiktionary\n"

	cd "$DIR"
	echo -e "\n\tDownloading “${FILE}”\n"
	wget "${args[@]}" "https://kaikki.org/dictionary/All%20languages%20combined/$FILE"

	headtail "$FILE"

	echo -e "\n\tGenerating dictionaries “${OUTPUT%.json}*.json”\n"
	python3 -X dev update.py ../all-locales.txt "$FILE" $OUTPUT

	head ${OUTPUT%.json}*.json

	mapfile -t codes < <(cut -f 1 languages.tsv)
	mapfile -t langs < <(cut -f 2 languages.tsv)
	declare -A LANGUAGES

	for i in "${!codes[@]}"; do
		LANGUAGES[${codes[i]}]=${langs[i]}
	done

	files=(${OUTPUT%.json}*.json)

	echo "| [Wiktionary](#wiktionary) | 🅭🅯🄎<br>CC BY-SA 3.0<br>GFDL | Weekly<br>$(outputupdate "$FILE") | $(outputfile "$DIR" "${files[0]}") |" >table.txt
	for file in "${files[@]:1}"; do
		echo "| | | | $(outputfile "$DIR" "$file") |"
	done >>table.txt

	# echo "| [Wiktionary](#wiktionary) | 🅭🅯🄎<br>CC BY-SA 3.0 | Weekly<br>$(outputupdate "$FILE") | $(outputfile "$DIR" ${OUTPUT%.json}*.json) |" > table.txt
)

table=''

for dir in "${DIRS[@]}"; do
	pushd "$dir" >/dev/null
	if [[ -e table.txt ]]; then
		table+=$(<table.txt)
		table+=$'\n'
		rm table.txt
	fi
	popd >/dev/null
done

README=$(<README.md.txt)
README=${README/TABLE/$table}

echo -e "\nDatabase comparison table:\n"
echo "$table"

echo "$README" >README.md

(
	OUTPUT=${OUTPUT%.json}

	shopt -s globstar
	echo "Dictionaries:"
	wc -l -- **/$OUTPUT*.json
	echo
	du -bch -- **/$OUTPUT*.json
)
