#!/usr/bin/env python3
# Teal Dulcet

# Run: python3 update.py <locales file> <input JSON file> <output JSON file>

# wget --compression auto -O all-locales-Firefox https://hg.mozilla.org/mozilla-central/raw-file/default/browser/locales/all-locales
# wget --compression auto -O all-locales-Firefox-Android https://hg.mozilla.org/mozilla-central/raw-file/default/mobile/android/locales/all-locales
# wget --compression auto -O all-locales-Thunderbird https://hg.mozilla.org/comm-central/raw-file/default/mail/locales/all-locales

# echo 'en-US' | sort -u - all-locales-* > all-locales.txt

# wget --compression auto https://kaikki.org/dictionary/All%20languages%20combined/kaikki.org-dictionary-all.jsonl

# time python3 -X dev update.py all-locales.txt kaikki.org-dictionary-all.jsonl dictionary.json

import csv
import json
import operator
import os
import re
import sys
import tempfile
import time
from collections import Counter
from contextlib import ExitStack
from datetime import timedelta
from urllib.parse import unquote, urlparse

if len(sys.argv) != 4:
	print(f"Usage: {sys.argv[0]} <locales file> <input JSON file> <output JSON file>", file=sys.stderr)
	sys.exit(1)

# Allowed Parts of speech
parts_of_speech = frozenset((
	"noun",
	"verb",
	"adj",
	"adv",
	"prep_phrase",
	"abbrev",
	"pron",
	"prep",
	"num",
	"conj",
	"det",
	"particle",
	"postp",
	"intj",
	"phrase",
	"name",
	"romanization",
))

# Allowed words, forms, synonyms and antonyms
# pattern = re.compile(r"^[\w'-]+$")
pattern = re.compile(r"^[\S]+$")

sep = "/"

n = 10

start = time.perf_counter()

with open(sys.argv[1], encoding="utf-8") as f:
	locales = [line.rstrip() for line in f]

languages = {locale.split("-", 1)[0] for locale in locales}

print(f"Mozilla languages: {len(languages):n}", sorted(languages))

# langs = {}
lang_codes = set()

keys = Counter()
part = Counter()
langs = Counter()
sense_tags = Counter()
ipa_tags = Counter()
audio_tags = Counter()
ogg_hosts = Counter()
mp3_hosts = Counter()
ogg_paths = Counter()
mp3_paths = Counter()
form_keys = Counter()
form_tags = Counter()
form_sources = Counter()

with open(sys.argv[2], "rb") as f:
	for line in f:
		d = json.loads(line)

		keys.update(d.keys())

		# lang = d["lang"]
		lang_code = d["lang_code"]

		lang_codes.add(lang_code)
		langs.update(((lang_code, d["lang"]),))

		part.update((d["pos"],))

		for sense in d["senses"]:
			if "tags" in sense:
				sense_tags.update(sense["tags"])

		if "forms" in d:
			for aform in d["forms"]:
				form_keys.update(aform.keys())
				if "tags" in aform:
					form_tags.update(aform["tags"])
				if "source" in aform:
					form_sources.update((aform["source"],))

				# form = aform["form"]
				# if ("tags" not in aform or {"inflection-template", "table-tags", "class"}.isdisjoint(aform["tags"])) and form and form != "-":
					# if form != form.strip():
					# print(f"Error: Form, strip, {form!r}", aform)

		if "sounds" in d:
			for s in d["sounds"]:
				if "ipa" in s and "tags" in s:
					ipa_tags.update(s["tags"])

				if "audio" in s:
					if "tags" in s:
						audio_tags.update(s["tags"])

					ogg_url = urlparse(s["ogg_url"])
					mp3_url = urlparse(s["mp3_url"])
					ogg_path = unquote(ogg_url.path).split(sep, 4)
					mp3_path = unquote(mp3_url.path).split(sep, 4)

					ogg_hosts.update((ogg_url.netloc,))
					mp3_hosts.update((mp3_url.netloc,))

					# ogg_paths.update((os.path.dirname(ogg_path),))
					ogg_paths.update((sep.join(ogg_path[:4]),))
					# mp3_paths.update((os.path.dirname(mp3_path),))
					mp3_paths.update((sep.join(mp3_path[:4]),))

print(f"Wiktionary languages: {len(lang_codes):n}", sorted(lang_codes))

diff = languages - lang_codes
print(f"Mozilla and Wiktionary languages difference: {len(diff):n}", sorted(diff))

languages &= lang_codes
print(f"Languages intersection: {len(languages):n}", sorted(languages))

end = time.perf_counter()
print(
	f"Total number of Languages: {len(langs):n}, Total number of Words: {sum(langs.values()):n}, Runtime: {timedelta(seconds=end - start)}"
)
start = end

akeys = Counter()
asenses = Counter()
aforms = Counter()

root, ext = os.path.splitext(sys.argv[3])
adir = os.path.dirname(sys.argv[3])

with ExitStack() as stack:
	files = {
		lang_code: stack.enter_context(tempfile.SpooledTemporaryFile(2 << 30, "w+b"))
		for (lang_code, _), count in langs.items()
		if lang_code in languages or count >= 50000
	}
	with open(sys.argv[2], "rb") as f:
		for line in f:
			d = json.loads(line)

			lang_code = d["lang_code"]
			if lang_code in files:
				files[lang_code].write(line)
		os.remove(sys.argv[2])

	astart = time.perf_counter()

	for (lang_code, lang), count in langs.items():
		if lang_code in languages or count >= 50000:
			words = {}
			file = files[lang_code]
			file.seek(0)
			for line in file:
				d = json.loads(line)

				word = d["word"]

				# if word != word.strip():
				# print(f"Error: Word, strip, {word!r}")

				if not pattern.match(word):
					continue

				pos = d["pos"]

				if pos not in parts_of_speech:
					continue

				if word not in words:
					words[word] = {"p": set(), "d": [], "f": set(), "s": set(), "n": set(), "i": [], "a": [], "w": []}

				data = words[word]
				# data[""].add(word)
				data["p"].add(pos)
				# data["d"] += (g for s in d["senses"] if "glosses" in s for g in s["raw_glosses" if "raw_glosses" in s else "glosses"])
				data["d"] += (s["raw_glosses" if "raw_glosses" in s else "glosses"][0] for s in d["senses"] if "glosses" in s)

				if "forms" in d:
					data["f"].update(
						form
						for form in (
							f["form"].strip()
							for f in d["forms"]
							if "tags" not in f or {"inflection-template", "table-tags", "class"}.isdisjoint(f["tags"])
						)
						if form and form != "-" and pattern.match(form)
					)

				if "synonyms" in d:
					data["s"].update(f["word"] for f in d["synonyms"] if pattern.match(f["word"]))

				if "antonyms" in d:
					data["n"].update(f["word"] for f in d["antonyms"] if pattern.match(f["word"]))

				if "sounds" in d:
					ipa_tag = any(
						"US" in t or "American" in t for s in d["sounds"] if "ipa" in s and "tags" in s for t in s["tags"]
					)
					audio_tag = any(
						"US" in t or "American" in t for s in d["sounds"] if "audio" in s and "tags" in s for t in s["tags"]
					)

					for s in d["sounds"]:
						if "ipa" in s and (not ipa_tag or "tags" not in s or any("US" in t or "American" in t for t in s["tags"])):
							data["i"].append(s["ipa"])

						if "audio" in s and (
							not audio_tag or "tags" not in s or any("US" in t or "American" in t for t in s["tags"])
						):
							ogg_url = urlparse(s["ogg_url"])
							ogg_path = unquote(ogg_url.path).split(sep, 3)
							data["a"].append(ogg_path[3])

				if "wikipedia" in d:
					data["w"] += d["wikipedia"]

			with tempfile.NamedTemporaryFile("w", dir=adir, encoding="utf-8", delete=False) as f:
				for word, data in sorted(words.items()):
					senses = list(dict.fromkeys(data["d"]))
					d = {"": word, "p": sorted(data["p"]), "d": senses}
					forms = data["f"]
					if forms:
						d["f"] = sorted(forms)
					if data["s"]:
						d["s"] = sorted(data["s"])
					if data["n"]:
						d["n"] = sorted(data["n"])
					if data["i"]:
						d["i"] = data["i"][0]
					if data["a"]:
						d["a"] = data["a"][0]
					if data["w"]:
						d["w"] = list(dict.fromkeys(data["w"]))

					akeys.update(d.keys())
					asenses.update((len(senses),))
					aforms.update((len(forms),))

					json.dump(d, f, ensure_ascii=False, separators=(",", ":"))
					f.write("\n")
				os.replace(f.name, f"{root}-{lang_code}{ext}")
			aend = time.perf_counter()
			print(
				f"Language: {lang} ({lang_code}),\tTotal number of Words: {len(words):n} / {count:n}, Runtime: {timedelta(seconds=aend - astart)}"
			)
			astart = aend
		elif count >= 10000:
			print(f"\tSkiping Language: {lang} ({lang_code}),\tTotal number of Words: {count:n}")

with open("languages.tsv", "w", newline="", encoding="utf-8") as csvfile:
	writer = csv.writer(csvfile, delimiter="\t", lineterminator="\n", quoting=csv.QUOTE_NONE)
	# writer.writerows((lang_code, lang) for (lang_code, lang), count in sorted(langs.items()) if lang_code in languages or count >= 50000)
	writer.writerows(sorted(langs))

end = time.perf_counter()
print(f"Total Runtime: {timedelta(seconds=end - start)}")

print("\nCounts\n")
print("Keys:", len(keys))
print("\n".join(f"\t{count}\t{key!r}" for key, count in keys.most_common()))

print("Part-of-speech:", len(part))
print("\n".join(f"\t{count}\t{pos}" for pos, count in part.most_common()))

print("Languages:", len(langs))
print("\n".join(f"\t{count}\t{lang}" for lang, count in langs.most_common(n * n)))

print("Sense Tags:", len(sense_tags))
print("\n".join(f"\t{count}\t{tag}" for tag, count in sense_tags.most_common(8 * n)))

print("IPA Tags:", len(ipa_tags))
print("\n".join(f"\t{count}\t{tag}" for tag, count in ipa_tags.most_common(4 * n)))

print("Audio Tags:", len(audio_tags))
print("\n".join(f"\t{count}\t{tag}" for tag, count in audio_tags.most_common(2 * n)))

print("ogg_hosts:", len(ogg_hosts))
print("\n".join(f"\t{count}\t{host}" for host, count in ogg_hosts.most_common()))

print("mp3_hosts:", len(mp3_hosts))
print("\n".join(f"\t{count}\t{host}" for host, count in mp3_hosts.most_common()))

print("ogg_paths:", len(ogg_paths))
print("\n".join(f"\t{count}\t{path}" for path, count in ogg_paths.most_common()))

print("mp3_paths:", len(mp3_paths))
print("\n".join(f"\t{count}\t{path}" for path, count in mp3_paths.most_common()))

print("Form Keys:", len(form_keys))
print("\n".join(f"\t{count}\t{key!r}" for key, count in form_keys.most_common()))

print("Form Tags:", len(form_tags))
print("\n".join(f"\t{count}\t{tag}" for tag, count in form_tags.most_common(n * n)))

print("Form Sources:", len(form_sources))
print("\n".join(f"\t{count}\t{source}" for source, count in form_sources.most_common()))

print("Keys:", len(akeys))
print("\n".join(f"\t{count}\t{key!r}" for key, count in akeys.most_common()))

print("Number of senses:", len(asenses))
print(
	"\n".join(
		f"\t{count}\t{senses}" for senses, count in sorted(asenses.most_common(2 * n), key=operator.itemgetter(1, 0), reverse=True)
	)
)

print("Number of forms:", len(aforms))
print(
	"\n".join(
		f"\t{count}\t{forms}" for forms, count in sorted(aforms.most_common(2 * n), key=operator.itemgetter(1, 0), reverse=True)
	)
)
