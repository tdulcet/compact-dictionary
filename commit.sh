#!/bin/bash

# Teal Dulcet
# Commit updated dictionaries
# ./commit.sh

set -e

if [[ $# -ne 0 ]]; then
	echo "Usage: $0" >&2
	exit 1
fi

# Maximum number of commits
COMMITS=10 # 5 * 2

# GitLab repository base URL
# URL=https://gitlab.com/tdulcet/compact-dictionary
URL=$CI_PROJECT_URL
# BRANCH=main
BRANCH=$CI_DEFAULT_BRANCH

date=$(date -u +%F)
message="Updated dictionaries $date."

if git diff --quiet; then
	exit 0
fi

git commit -am "$message"

if [[ $(git rev-list --count HEAD) -gt $COMMITS ]]; then
	# git rev-parse HEAD~$((COMMITS-1)) > .git/info/grafts
	# git filter-branch
	hash=$(git rev-parse HEAD~$((COMMITS - 1)))
	git checkout --orphan temp "$hash"
	git commit -am "Squashed old commits."
	git rebase --onto temp "$hash" "$BRANCH"
	# git branch -d temp
fi

changelog="### $date"$'\n'"- [$message]($URL/-/commit/$(git rev-parse HEAD))"

CHANGELOG=$(<CHANGELOG.md)
CHANGELOG=${CHANGELOG/Unreleased/Unreleased$'\n\n\n'"$changelog"}

echo -e "\nCHANGELOG:\n"
echo "$changelog"

echo "$CHANGELOG" >CHANGELOG.md

git commit -am "Updated CHANGELOG."
